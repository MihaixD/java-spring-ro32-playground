package com.example.myfirstproject.ticket_machine.controller;

import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.annotation.SessionScope;

import java.io.IOException;

@RestController
@SessionScope
@RequestMapping("tickets")
public class TicketController {
    private int currentTicketId;

    public TicketController() {
        currentTicketId = 0;
    }

    @GetMapping
    public int getNextTicketNumber() {
        if (currentTicketId == 5) {
            throw new RuntimeException("ID 5 is not allowed.");
        }

        currentTicketId++;
        return currentTicketId;
    }
}
