package com.example.myfirstproject;

import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;

@Configuration
public class SecurityConfig extends WebSecurityConfigurerAdapter {
    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.authorizeRequests()
                .antMatchers("/api/files-data/**").hasAnyRole("ADMIN")
                .antMatchers(HttpMethod.POST, "/persons/view").hasAnyRole("ADMIN")
                .antMatchers("/persons/**").hasAnyRole("USER")
                .antMatchers("/tickets").permitAll()
                .antMatchers(HttpMethod.POST, "/api/cars").hasAnyRole("ADMIN", "CARS")
                .antMatchers(HttpMethod.GET, "/api/cars").authenticated()
                .antMatchers("/api/users/**").hasAnyAuthority("ROLE_USER_ADMIN")
                .antMatchers("/").permitAll()
                .anyRequest().authenticated()
                .and()
                .formLogin()
                .and()
                .logout()
                .and()
                .httpBasic();
    }

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        // In real apps -> don't use {noop}. In real products, we should hash passwords to avoid vulnerabilities.
        auth.inMemoryAuthentication()
                .withUser("user1").password("{noop}user1").roles("USER")
                .and()
                .withUser("admin").password("{noop}admin").roles("USER", "ADMIN");
    }
}
