package com.example.myfirstproject;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
public class MyfirstprojectApplication {

    public static void main(String[] args) {
        SpringApplication.run(MyfirstprojectApplication.class, args);
    }

}