package com.example.myfirstproject.file_data.service;

import com.example.myfirstproject.file_data.model.FileData;
import com.example.myfirstproject.file_data.repository.FileDataRepository;
import com.google.common.collect.ImmutableList;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;


@Service
@Transactional
public class MockDataFileDataService {
    private final FileDataRepository repository;
    private List<FileData> cachedMockData;

    public MockDataFileDataService(FileDataRepository repository) {
        this.repository = repository;
    }

    public void setupMockData(int nElements) {
        List<FileData> elements = new ArrayList<>();
        for (int i = 0;i < nElements; i++) {
            elements.add(createMockObject(i));
        }
        cachedMockData = ImmutableList.copyOf(repository.saveAll(elements));
    }

    public void cleanUpMockData() {
        repository.deleteAll(cachedMockData);
        cachedMockData = List.of();
    }

    private FileData createMockObject(int n) {
        FileData fileData = new FileData();
        fileData.setFileName("MockFile " + n);
        fileData.setContent("Mock content " + n);
        fileData.setSizeInKb(n);
        fileData.setExtension("txt");
        return fileData;
    }
}
