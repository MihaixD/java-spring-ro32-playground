package com.example.myfirstproject.file_data.service;

import com.example.myfirstproject.file_data.model.FileData;
import com.example.myfirstproject.file_data.model.SdaNotFoundException;
import com.example.myfirstproject.file_data.repository.FileDataRepository;
import com.google.common.collect.ImmutableList;
import org.springframework.stereotype.Service;

import java.io.File;
import java.util.List;

@Service
public class FileDataService {
    private FileDataRepository fileDataRepository;

    public FileDataService(FileDataRepository fileDataRepository) {
        this.fileDataRepository = fileDataRepository;
    }

    public List<FileData> findAll() {
        return ImmutableList.copyOf(fileDataRepository.findAll());
    }

    public FileData getById(Long id) {
        return fileDataRepository.findById(id)
                .orElseThrow(() -> new SdaNotFoundException("File Data"));
    }

    public FileData add(FileData fileData) {
        return fileDataRepository.save(fileData);
    }

    public FileData update(FileData fileData, Long id) {
        fileDataRepository.findById(id)
                .orElseThrow(() -> new SdaNotFoundException("File Data"));
        fileData.setId(id);
        return fileDataRepository.save(fileData);
    }

    public void delete(Long id) {
        fileDataRepository.findById(id)
                        .orElseThrow(() -> new SdaNotFoundException("File Data"));
        fileDataRepository.deleteById(id);
    }

    public List<FileData> findAllBySizeInKbGreaterThan(int size){
        return fileDataRepository.findAllBySizeInKbGreaterThan(size);
    }

    public List<FileData> findAllByExtensionAndPrefix(String extension, String fileNamePrefix){
        return fileDataRepository.findAllByExtensionAndFileNameStartingWith(extension, fileNamePrefix);
    }
}
