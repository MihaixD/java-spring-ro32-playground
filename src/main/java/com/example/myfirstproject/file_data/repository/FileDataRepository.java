package com.example.myfirstproject.file_data.repository;

import com.example.myfirstproject.file_data.model.FileData;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface FileDataRepository extends CrudRepository<FileData, Long> {

        List<FileData> findAllBySizeInKbGreaterThan(int size);

        List<FileData> findAllByExtensionAndFileNameStartingWith(@Param("extension") String  extension, @Param("fileName") String prefix);
}
