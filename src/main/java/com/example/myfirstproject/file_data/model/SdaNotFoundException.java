package com.example.myfirstproject.file_data.model;

public class SdaNotFoundException extends RuntimeException {
    public SdaNotFoundException(String message) {
        super(message);
    }
}
