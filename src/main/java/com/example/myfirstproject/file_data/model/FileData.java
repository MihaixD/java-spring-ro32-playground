package com.example.myfirstproject.file_data.model;

import com.example.myfirstproject.person.model.Person;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;

import javax.persistence.*;

@Data
@Entity
public class FileData {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @ManyToOne
    @JoinColumn(name = "person_id")
    @JsonIgnore
    private Person ownerPerson;

    @Column(name = "file_name")
    private String fileName;

    @Column(name = "extension")
    private String extension;

    @Column(name = "size_in_kb")
    private Integer sizeInKb;

    @Column(name = "content")
    private String content;

//    @Transient
//    private Integer sizeInMb;
}
