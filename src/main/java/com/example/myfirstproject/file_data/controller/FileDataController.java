package com.example.myfirstproject.file_data.controller;

import com.example.myfirstproject.file_data.model.FileData;
import com.example.myfirstproject.file_data.service.FileDataService;
import com.example.myfirstproject.file_data.service.MockDataFileDataService;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.util.List;

@Controller
@RequestMapping("/api/files-data")
public class FileDataController {
    private MockDataFileDataService mockDataFileDataService;
    private FileDataService fileDataService;

    public FileDataController(FileDataService fileDataService, MockDataFileDataService mockDataFileDataService) {
        this.fileDataService = fileDataService;
        this.mockDataFileDataService = mockDataFileDataService;
    }

    @GetMapping
    @ResponseBody
    @ResponseStatus(HttpStatus.OK)
    public List<FileData> findAll() {
        return fileDataService.findAll();
    }

    @GetMapping("/{id}")
    @ResponseBody
    @ResponseStatus(HttpStatus.OK)
    public FileData getById(@PathVariable Long id) {
        return fileDataService.getById(id);
    }

    @PostMapping
    @ResponseBody
    @ResponseStatus(HttpStatus.CREATED)
    public ResponseEntity<FileData> add(@RequestBody FileData fileData) {
        FileData fileDataCreated = fileDataService.add(fileData);
        return ResponseEntity.ok()
                .location(URI.create("/api/files-data/" + fileDataCreated.getId()))
                .body(fileDataCreated);
    }

    @PutMapping("/{id}")
    @ResponseBody
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public FileData update(@RequestBody FileData fileData, @PathVariable Long id) {
        return fileDataService.update(fileData, id);
    }

    @DeleteMapping ("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void delete(@PathVariable Long id) {
        fileDataService.delete(id);
    }


    @PostMapping("/mocks/{nToCreate}")
    @ResponseStatus(HttpStatus.CREATED)
    public void createMocks(@PathVariable int nToCreate) {
        mockDataFileDataService.setupMockData(nToCreate);
    }

    @DeleteMapping("/mocks")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deleteMocks() {
        mockDataFileDataService.cleanUpMockData();
    }

    @GetMapping("/size-greater-than/{size}")
    @ResponseBody
    public List<FileData> findAllBySizeInKbGreaterThan(@PathVariable int size){
        return fileDataService.findAllBySizeInKbGreaterThan(size);
    }

    // requestparam used when having multiple arguments (no longer need to specify it in the path)
    @GetMapping("/name-starts-with")
    @ResponseBody
    public List<FileData> findByExtensionAndPrefix(@RequestParam String extension, @RequestParam String prefix){
        return fileDataService.findAllByExtensionAndPrefix(extension, prefix);
    }

    @GetMapping("/view")
    public String getFileDataAllView(ModelMap modelMap) {
        List<FileData> fileDataEntries = fileDataService.findAll();
        modelMap.addAttribute("elements", fileDataEntries);
        return "file-data-all-view";
    }

    @GetMapping("/view/{id}")
    public String getFileDataView(@PathVariable Long id, ModelMap modelMap) {
        FileData fileData = fileDataService.getById(id);
        modelMap.addAttribute("element", fileData);
        return "file-data-view";
    }

}
