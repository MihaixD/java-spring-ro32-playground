package com.example.myfirstproject.person.service;

import com.example.myfirstproject.person.model.Person;

import java.util.List;
import java.util.Map;

public interface PersonService {
    Person addPerson(Person person);

    List<Person> getPersons();

    Person updatePerson(Person person);

    void deletePerson(Long id);

    Person getPersonByFirstName(String firstName);

    Map<String, List<Person>> getPersonsByAgeGroup();

    List<Person> getPersonsByAgeAfter(int age);

    Person getPersonById(Long id);
}
