package com.example.myfirstproject.person.service;

import com.example.myfirstproject.file_data.model.FileData;
import com.example.myfirstproject.file_data.repository.FileDataRepository;
import com.example.myfirstproject.person.model.Person;
import com.example.myfirstproject.person.repository.PersonRepository;
import com.google.common.collect.ImmutableList;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class PersonServiceImpl implements PersonService {
    @Autowired
    PersonRepository personRepository;
    @Autowired
    FileDataRepository fileDataRepository;

    @Override
    public Person addPerson(Person person) {
        // person -> files (link already exists) -> this will create some entries in the files table

        //add file -> person link (for each file in the list)
        person.getFiles().forEach(file -> file.setOwnerPerson(person));

        return personRepository.save(person);
    }

    // Example of how to persist multiple tables (without implicit cascade rules).
    // This should be a transaction - either update both tables or none.
    @Transactional
    public void addPersonWithoutCascadeAnnotation(Person person) {
        // get the person with the ID set by the database
        Person savedPerson = personRepository.save(person);

        for(FileData file: person.getFiles()) {
            // file.person_id = Person.id
            file.setOwnerPerson(savedPerson);
            fileDataRepository.save(file);
        }
    }

    @Override
    public List<Person> getPersons() {
        return ImmutableList.copyOf(personRepository.findAll());
    }

    @Override
    public Person updatePerson(Person person) {
        return personRepository.save(person);
    }

    @Override
    public void deletePerson(Long id) {
        personRepository.deleteById(id);
    }

    @Override
    public Person getPersonByFirstName(String firstName) {
        return personRepository.findByFirstNameIgnoreCase(firstName)
                .orElseThrow(() -> new NoSuchElementException("No person with '" + firstName + "' first name found !"));
    }

    @Override
    public Map<String, List<Person>> getPersonsByAgeGroup() {
        return this.getPersons().stream()
                .collect(Collectors.groupingBy(person -> getAgeGroup(person)));
    }
    @Override
    public List<Person> getPersonsByAgeAfter(int age){
        return personRepository.findAllByAgeAfter(age);
    }

    @Override
    public Person getPersonById(Long id) {
        return personRepository.findById(id).orElseThrow();
    }

    private String getAgeGroup(Person person) {
        Integer ageWithFirstDigit = person.getAge() / 10;
        Integer minAgeGroup = ageWithFirstDigit * 10;
        Integer maxAgeGroup = minAgeGroup + 9;
        return minAgeGroup + "-" + maxAgeGroup;
    }
}
