package com.example.myfirstproject.person.model;

import com.example.myfirstproject.file_data.model.FileData;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.*;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
public class Person {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long id;

    @NonNull
    private String firstName;

    @NonNull
    private String lastName;

    @NonNull
    private Integer age;

    @OneToMany(mappedBy="ownerPerson", cascade = {CascadeType.ALL})
    private List<FileData> files = new ArrayList<>();

    public void copyFromPerson(Person person) {
        this.setFirstName(person.getFirstName());
        this.setLastName(person.getLastName());
        this.setAge(person.getAge());
    }
}
