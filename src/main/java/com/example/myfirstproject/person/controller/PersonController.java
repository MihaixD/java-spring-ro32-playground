package com.example.myfirstproject.person.controller;

import com.example.myfirstproject.person.model.Person;
import com.example.myfirstproject.person.service.PersonService;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/persons")
public class PersonController {
    private PersonService personService;

    public PersonController(PersonService personService) {
        this.personService = personService;
    }

    @GetMapping
    public List<Person> getPersons() {
        return personService.getPersons();
    }

    @PostMapping
    public void addPerson(@RequestBody Person person) {
        personService.addPerson(person);
    }

    @GetMapping("/ageGroup")
    public Map<String, List<Person>> getPersonsByAge() {
        return personService.getPersonsByAgeGroup();
    }

    @GetMapping("/age_after/{age}")
    public List<Person> getPersonsByAgeAfter(@PathVariable int age){
        return personService.getPersonsByAgeAfter(age);
    }

    @PutMapping
    public Person updatePerson(@RequestBody Person person) {
        return personService.updatePerson(person);
    }

    @DeleteMapping("/{id}")
    public void deletePerson(@PathVariable Long id) {
        personService.deletePerson(id);
    }
}
