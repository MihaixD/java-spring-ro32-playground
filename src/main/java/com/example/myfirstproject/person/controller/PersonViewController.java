package com.example.myfirstproject.person.controller;

import com.example.myfirstproject.person.model.Person;
import com.example.myfirstproject.person.service.PersonService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@Controller
@RequestMapping("/persons/view")
public class PersonViewController {
    private final PersonService personService;

    public PersonViewController(PersonService personService) {
        this.personService = personService;
    }

    @GetMapping
    public String getFileDataAllView(ModelMap modelMap) {
        List<Person> persons = personService.getPersons();
        modelMap.addAttribute("persons", persons);
        modelMap.addAttribute("personToAdd", mockPerson());
        return "person-all-view";
    }

    @GetMapping("/{id}")
    public String getFileDataView(@PathVariable Long id, ModelMap modelMap) {
        Person person = personService.getPersonById(id);
        modelMap.addAttribute("person", person);
        return "person-view";
    }

    @PostMapping
    public String addPerson(@ModelAttribute("personToAdd") Person person, ModelMap modelMap) {
        // Alternative to @ModelAttribute. "getAttribute" -> extract content from modelmap
        Person personFromView = (Person)modelMap.getAttribute("personToAdd");

        Person savedPerson = personService.addPerson(person);

        // Redirect to (single) person view and display the newly created person.
        modelMap.addAttribute("person", savedPerson);
        return "person-view";
    }

    private Person mockPerson() {
        Person person = new Person();
        person.setFirstName("FirstName");
        person.setLastName("LastName");
        person.setAge(20);
        person.setFiles(new ArrayList<>());
        return person;
    }
}
