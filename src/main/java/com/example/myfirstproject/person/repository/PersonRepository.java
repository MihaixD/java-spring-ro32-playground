package com.example.myfirstproject.person.repository;

import com.example.myfirstproject.person.model.Person;
import org.springframework.data.repository.CrudRepository;

import java.util.List;
import java.util.Optional;

public interface PersonRepository extends CrudRepository<Person, Long> {
    Optional<Person> findByFirstNameIgnoreCase(String firstName);

    List<Person> findAllByAgeAfter(int age);

    List<Person> findAllByAgeAfterOrderByFirstName(int age);
}
