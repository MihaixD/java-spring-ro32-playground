package com.example.myfirstproject.exception_handler;

import com.example.myfirstproject.file_data.model.SdaNotFoundException;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import java.util.NoSuchElementException;

@RestControllerAdvice
public class GlobalExceptionHandler extends ResponseEntityExceptionHandler {
    @ExceptionHandler
    public String handleException(RuntimeException e) {

        return "I received an exception!" + e.getMessage();
    }

    @ExceptionHandler
    public ResponseEntity<Object> handleException(SdaNotFoundException e, WebRequest webRequest) {
        return this.handleExceptionInternal(
                e,
                "Item not found by ID !",
                HttpHeaders.EMPTY,
                HttpStatus.BAD_REQUEST,
                webRequest
                );
    }

    @ExceptionHandler
    public String handleException(NoSuchElementException e) {
        return "The requested resource does not exist!";
    }
}
