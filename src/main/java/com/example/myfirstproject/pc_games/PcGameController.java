package com.example.myfirstproject.pc_games;

import com.example.myfirstproject.person.model.Person;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.ArrayList;

@Controller
@RequestMapping("/pc-games")
public class PcGameController {

    @GetMapping
    public String showForm(ModelMap modelMap) {
        modelMap.addAttribute("pcGameToAdd", mockGame());
        return "pc-game-form.html";
    }

    @PostMapping
    public String showPcGameDisplayInfo(@ModelAttribute("pcGameToAdd") PcGame pcGame, ModelMap modelMap) {
        modelMap.addAttribute("pcGame", pcGame);
        return "pc-game-info.html";
    }

    private PcGame mockGame() {
        PcGame pcGame = new PcGame();
        pcGame.setProducer("producer");
        pcGame.setTitle("title");
        return pcGame;
    }

}
