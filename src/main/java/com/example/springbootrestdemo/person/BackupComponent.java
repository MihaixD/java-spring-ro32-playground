package com.example.springbootrestdemo.person;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@Component
@Slf4j
public class BackupComponent implements CommandLineRunner {
    private static final String BACKUP_FILE_NAME = "backup.json";
    private static final File BACKUP_FILE = new File(BACKUP_FILE_NAME);

    private final TypeReference<List<String>> stringListReference = new TypeReference<>(){};
    List<String> words = new ArrayList<>();
    ObjectMapper objectMapper = new ObjectMapper();

    @Override
    public void run(String... args) throws Exception {
        log.info("Started backup component");
        words.addAll(List.of("A", "B", "C", "D"));
        log.info("Initialized words list: {}", words);
        saveToFile();
        words.clear();
        log.info("Cleaned up words list: {}", words);
        loadFromFile();
        log.info("Loaded backup for words: {}", words);
    }

    public void loadFromFile() {
        try {
            words = objectMapper.readValue(BACKUP_FILE, stringListReference);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void saveToFile() {
        try {
            objectMapper.writeValue(BACKUP_FILE, words);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


}
